import * as React from 'react';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import { CircularProgress } from '@material-ui/core';
import { Error } from '@material-ui/icons';
import { red } from "@material-ui/core/colors";
import { State } from '../types/note';
import { useDataApi } from '../models/note';
import { Article, NotesAppBar } from '../components';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    center: {
      display: "flex",
      alignItems: "center",
      justifyContent: "center",
      height: "90vh",
    },
    error: {
      fontSize: theme.spacing(30),
      color: red[500],
    },
  }),
);

const NoteApp: React.FC = () => {
  const classes = useStyles();
  const [{ data, isLoading, isError }] = useDataApi('notes') as [State, () => void]; // , setUrl
  // setUrl("")

  return (
    <div>
      <NotesAppBar />
      {isError && <div className={classes.center}><Error className={classes.error} /></div>}
      {isLoading ? (
        <div className={classes.center}>
          <CircularProgress />
        </div>
      ) : (
        <Article data={data} />
      )}
    </div>
  );
}

export default NoteApp;
