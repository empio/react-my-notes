import * as React from 'react';
import { Button} from '@material-ui/core';
import { AuthContext } from '../utils/auth';
import history from '../utils/history';

const Login: React.FC = () => {
  const [, , handleLogin] = React.useContext(AuthContext) as any;
  const handleClick = () => {
    handleLogin({user: "piotrek"});
    history.push("/");
  };
  return (
    <div
      style={{
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        height: "100vh"
      }}
    >
      <Button
        color="primary"
        variant="contained"
        onClick={() => handleClick()}
      >
        Login
      </Button>
    </div>
  );
}

export default Login;
