import * as timerUitl from "../utils/timer";
import { Note } from "../types/note";

function notes(): Note[] {
  return [
    {
      id: "1",
      name: "Lorem ipsum dolor sit amet...",
      author: "Anonim",
      summary: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer quis rhoncus est, non iaculis dolor. Fusce non mauris vel lorem ornare tincidunt ac at nunc. Morbi risus ligula, tristique in fermentum id, tincidunt sed arcu. Ut sed diam leo. Aenean suscipit justo sapien, non tristique nulla facilisis eu. Fusce viverra auctor libero id imperdiet. Donec id varius erat, id cursus leo. Aliquam tristique laoreet sem, ac malesuada dolor. Curabitur eros eros, euismod at mi eu, elementum cursus nibh. Vivamus et tellus est. Vivamus erat arcu, scelerisque nec enim et, maximus auctor sem. Donec ipsum eros, mattis sit amet rutrum vel, feugiat at quam. Maecenas volutpat lacus id neque maximus porttitor. Mauris id orci id sem gravida ornare.",
      publishTime: "17.11.2019, 07:00:25"
    },
    {
      id: "2",
      name: "Integer mollis lobortis erat luctus...",
      author: "Anonim",
      summary: "Integer mollis lobortis erat luctus mattis. Duis non malesuada quam, vel ornare arcu. In hac habitasse platea dictumst. Aenean vitae dolor odio. Quisque quis dapibus justo, vitae vulputate tellus. Fusce ac dapibus nisl. Vestibulum consequat condimentum dictum. Maecenas in orci in tortor ornare facilisis. Mauris at consequat nulla. Aliquam semper elit vel turpis euismod, ac suscipit diam ultricies. Nullam sagittis sapien ut ex feugiat, porta auctor dolor lobortis.",
      publishTime: "18.11.2019, 07:00:25"
    },
    {
      id: "3",
      name: "Donec non sagittis lectus. Maecenas...",
      author: "Anonim",
      summary: "Donec non sagittis lectus. Maecenas sit amet leo semper, viverra leo ac, aliquam leo. Etiam non neque ultricies, lobortis ante eget, commodo lorem. Nam auctor nisi eget finibus lobortis. Sed rutrum cursus felis, sed finibus erat consequat et. Donec ullamcorper nec lacus ut tempus. Etiam sed diam et odio bibendum pellentesque. Phasellus lacus velit, molestie finibus felis nec, elementum accumsan elit. Nulla sollicitudin eros nec erat hendrerit, eu hendrerit purus vehicula. Morbi et purus ultricies, tincidunt ipsum in, placerat sem. Etiam ut consectetur neque.",
      publishTime: "19.11.2019, 07:00:25"
    }
  ];
}

export async function getNotes() {
  await timerUitl.delay(5000);
  let loopCount = 10;
  let books: Note[] = [];
  while (loopCount > 0) {
    books = books.concat(notes());
    loopCount--;
  }

  return {
    code: 0,
    data: books
  };
}
