import * as React from 'react';
import { Switch, Route, Router } from 'react-router-dom';
import { PrivateRoute } from './components';
import history from './utils/history';
import NoteApp from './pages/NoteApp';
import Login from './pages/Login';

// function PrivateRoute ({
//   component: Component,
//   authenticated,
//   ...rest
// } : {
//   authenticated: string | null,
//   path: string;
//   component: React.FC<RouteComponentProps & { children?: React.ReactNode; }>
// }) {
//   return (
//     <Route
//       {...rest}
//       render={(props) => authenticated
//         ? <Component {...props} />
//         : <Redirect to={{pathname: '/login', state: {from: props.location}}} />}
//     />
//   )
// }

const App: React.FC = () => {
  return (
    <Router history={history}>
      <Switch>
        <Route path="/login" component={Login} />
        {/* <PrivateRoute authenticated={token} path='/' component={NoteApp} /> */}
        <PrivateRoute exact path="/" component={NoteApp} />
      </Switch>
    </Router>
  );
}

export default App;
