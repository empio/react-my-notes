interface ObjectBase {
  id: string;
}

export interface Note extends ObjectBase {
  name: string;
  author: string;
  summary: string;
  publishTime: string;
}

export type Actions =
  | { type: "FETCH_INIT" }
  | { type: "FETCH_SUCCESS", payload: Note[] }
  | { type: "FETCH_FAILURE" };

export type State = {
  isLoading: boolean;
  isError: boolean;
  data: Note[];
};
