import * as React from 'react';
import {
  Avatar,
  Card,
  CardContent,
  CardHeader,
  Chip,
  Divider,
  Typography
} from '@material-ui/core';
import { Note } from '../../types/note';
import useStyles from './article.styles';

interface Props {
  data: Note[]
}

const Article: React.FC<Props> = ({data}) => {
  const classes = useStyles();

  return (
    <div className={classes.container}>
      {data.map((item, index) => (
        <Card className={classes.card} key={index}>
          <CardHeader
            avatar={
              <Avatar className={classes.avatar}>{item.id}</Avatar>
            }
            subheader={item.author}
            title={item.name}
          />
          <CardContent>
            <Typography>{item.summary}</Typography>
            <Divider className={classes.divider} />
            <Typography display="inline">publish at: </Typography>
            <Chip label={item.publishTime} size="small" />
          </CardContent>
        </Card>
      ))}
    </div>
  );
}

export default Article;
