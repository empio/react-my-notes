import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { blue } from '@material-ui/core/colors';

export default makeStyles((theme: Theme) =>
  createStyles({
    container: {
      maxWidth: 800,
      margin: "auto",
    },
    card: {
      margin: `${theme.spacing(5)}px 0`,
    },
    avatar: {
      backgroundColor: blue[500],
    },
    divider: {
      margin: `${theme.spacing(3)}px 0`,
    },
  }),
);