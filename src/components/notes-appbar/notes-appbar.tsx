import * as React from 'react';
import { AppBar, Button, Toolbar, Typography } from '@material-ui/core';
import { Error, CheckCircle, Loop } from '@material-ui/icons';
import useToast from '../../utils/use-toast';
import { State } from '../../types/note';
import { useDataApi } from '../../models/note';
import { AuthContext } from '../../utils/auth';
import history from '../../utils/history';

const NotesAppBar: React.FC = () => {
  const [, , , handleLogout] = React.useContext(AuthContext) as any;
  const [{ isLoading, isError }] = useDataApi('') as [State, () => void];
  const { showToast } = useToast();
  const [isShowToast, setShowToast] = React.useState(true);
  React.useEffect(
    () => {
      if (isShowToast) {
        showToast('Success', 'success');
        setShowToast(false);
      }
    },
    [isShowToast, showToast]
  );
  const handleClick = () => {
    handleLogout();
    history.push("/login");
  };
  return (
    <AppBar style={{ flexGrow: 1 }} position="static">
      <Toolbar>
        {isLoading ? (
          <Loop style={{ marginRight: 16 }} />
        ) : (
          <CheckCircle style={{ marginRight: 16 }} />
        )}
        <div style={{ flexGrow: 1 }}>
          <Typography variant="h6">
            Notes
          </Typography>
          {isError && <Error style={{ marginLeft: 16 }} />}
        </div>
        <Button
          color="inherit"
          onClick={() => handleClick()}
        >
          Logout
        </Button>
      </Toolbar>
    </AppBar>
  );
}

export default NotesAppBar;
