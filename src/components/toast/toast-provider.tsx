import * as React from 'react';
import { CheckCircle } from '@material-ui/icons';
import { SnackbarProvider } from 'notistack';

type Props = {};

const ToastProvider: React.FC<Props> = ({ children }) => {
  return (
    <SnackbarProvider
      maxSnack={3}
      dense
      anchorOrigin={{
        vertical: "top",
        horizontal: "right"
      }}
      iconVariant={{ success: <CheckCircle style={{ marginRight: 16 }} /> }}
    //   classes={{
    //     containerAnchorOriginTopRight: classes...,
    //     variantSuccess: classes...,
    //     variantError: classes...
    //   }}
    >
      {children}
    </SnackbarProvider>
  );
};

export default ToastProvider;
