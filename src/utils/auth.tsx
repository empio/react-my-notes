import * as React from 'react';
import { useState, useEffect, createContext } from 'react';

export const AuthContext = createContext(null);

export const AuthProvider = (props: { children: React.ReactNode; }) => {
  const storedUser = JSON.parse(window.localStorage.getItem("user") as string) || null;
  const [currentUser, setCurrentUser] = useState(storedUser);

  useEffect(() => {
    window.localStorage.setItem("user", JSON.stringify(currentUser));
  }, [currentUser]);

  const handleLogin = (data: {user: string}) => {
    setCurrentUser(data);
  };

  const handleLogout = () => {
    window.localStorage.removeItem("user");
    setCurrentUser(null);
  };

  return (
    <AuthContext.Provider
      value={[currentUser, setCurrentUser, handleLogin, handleLogout] as any}
    >
      {props.children}
    </AuthContext.Provider>
  );
};
