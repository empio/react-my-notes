// import { createStore } from "redux";
// import rootReducer from "../reducers/index";

// const store = createStore(rootReducer);

// export default store;

import { createStore, applyMiddleware } from "redux";
import rootReducer from "../reducers/index";
import { forbiddenWordsMiddleware } from "../middleware";

const store = createStore(
  rootReducer,
  applyMiddleware(forbiddenWordsMiddleware)
);

export default store;
