// {
//   type: 'ADD_ARTICLE',
//   payload: { title: 'React Redux Tutorial', id: 1 }
// }

// export function addArticle(payload) {
//   return { type: "ADD_ARTICLE", payload }
// };

import { ADD_ARTICLE } from "../constants/action-types";

export function addArticle(payload) {
  return { type: ADD_ARTICLE, payload };
}
