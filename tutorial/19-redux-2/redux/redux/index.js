import store from "./store/index";
import { addArticle } from "./actions/index";

window.store = store;
window.addArticle = addArticle;

// store.getState();
// output: {articles: Array(0)}

// store.subscribe(() => console.log('Look ma, Redux!!'));

// store.dispatch( addArticle({ title: 'React Redux Tutorial for Beginners', id: 1 }) );

// store.getState();
// output: {articles: Array(1)}
