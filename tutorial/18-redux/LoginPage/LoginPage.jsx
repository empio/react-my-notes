import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import { userActions } from '../actions';

function LoginPage() {
  const [inputs, setInputs] = useState({
    username: '',
    password: ''
  });
  const [submitted, setSubmitted] = useState(false);
  const { username, password } = inputs;
  const loggingIn = useSelector(state => state.authentication.loggingIn);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(userActions.logout());
  }, []);

  function handleChange(e) {
    const { name, value } = e.target;
    setInputs(inputs => ({ ...inputs, [name]: value }));
  }

  function handleSubmit(e) {
    e.preventDefault();

    setSubmitted(true);
    if (username && password) {
      dispatch(userActions.login(username, password));
    }
  }

  return (
    <div>
      <h2>Login</h2>
      <form name="form" onSubmit={handleSubmit}>
        <div>
          <label>Username</label>
          <input type="text" name="username" value={username} onChange={handleChange} />
          {submitted && !username &&
            <div>Username is required</div>
          }
        </div>
        <div>
          <label>Password</label>
          <input type="password" name="password" value={password} onChange={handleChange} />
          {submitted && !password &&
            <div>Password is required</div>
          }
        </div>
        <div>
          <button>
            {loggingIn && <span></span>}
            Login
          </button>
          <Link to="/register">Register</Link>
        </div>
      </form>
    </div>
  );
}

export { LoginPage };
