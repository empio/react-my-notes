import React, { useEffect } from 'react';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import { userActions } from '../actions';

function HomePage() {
  const users = useSelector(state => state.users);
  const user = useSelector(state => state.authentication.user);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(userActions.getAll());
  }, []);

  function handleDeleteUser(id) {
    dispatch(userActions.delete(id));
  }

  return (
    <div>
      <h1>Hi {user.firstName}!</h1>
      <h3>All registered users:</h3>
      {users.loading && <span>Loading users...</span>}
      {users.error && <span>ERROR: {users.error}</span>}
      {users.items &&
        <ul>
          {users.items.map((user, index) =>
            <li key={user.id}>
              {user.firstName + ' ' + user.lastName}
              {
                user.deleting ? <span> - Deleting...</span>
                : user.deleteError ? <span> - ERROR: {user.deleteError}</span>
                : <span> - <a onClick={() => handleDeleteUser(user.id)}>Delete</a></span>
              }
            </li>
          )}
        </ul>
      }
      <p>
        <Link to="/login">Logout</Link>
      </p>
    </div>
  );
}

export { HomePage };
