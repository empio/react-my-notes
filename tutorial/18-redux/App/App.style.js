import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles({
  cardRoot: {
    minWidth: 275,
  },
});
