import * as React from "react";
import { MemoryRouter } from "react-router-dom";
import { MemoryRouterProps } from "react-router";

type Props = MemoryRouterProps;

const MockRouter: React.FC<Props> = ({ children, ...routerProps }) => (
  <MemoryRouter {...routerProps}>{children}</MemoryRouter>
);

export default MockRouter;
