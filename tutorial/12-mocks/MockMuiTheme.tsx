import * as React from "react";
import { Theme } from "@material-ui/core";
import { ThemeProvider } from "@material-ui/styles";

import projectTheme from "config/theme";

type Props = {
  theme?: Theme;
};

const MockMuiTheme: React.FC<Props> = ({ children, theme = projectTheme }) => (
  <ThemeProvider theme={theme}>{children}</ThemeProvider>
);

export default MockMuiTheme;
