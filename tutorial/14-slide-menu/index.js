import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import MenuContainer from "./MenuContainer";
 
ReactDOM.render(
  <MenuContainer/>, 
  document.querySelector("#menu")
);

// #theMenu {
//   position: fixed;
//   left: 0;
//   top: 0;
//   transform: translate3d(-100vw, 0, 0);
//   width: 100vw;
//   height: 100vh;
// }
// transform: translate3d(0vw, 0, 0);
// transform: translate3d(-100vw, 0, 0);
// transition: transform .3s cubic-bezier(0, .52, 0, 1);
