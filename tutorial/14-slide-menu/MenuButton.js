import React, { PureComponent } from "react";
import './MenuButton.css';
class MenuButton extends PureComponent {
  render() {
    console.log("Rendering: MenuButton");
    return <button id="roundButton" onMouseDown={this.props.handleMouseDown} />;
  }
}
export default MenuButton;

// shouldComponentUpdate(nextProps, nextState) {
//   return false;
// }

// shouldComponentUpdate(nextProps, nextState) {
//   if (nextProps.handleMouseDown === this.props.handleMouseDown) {
//     return false;
//   } else {
//     return true;
//   }
// }

// class Blah extends Component {
//   render() {
//     return <p>Hello!</p>;
//   }
// }

// class Blah extends PureComponent {
//   render() {
//     return <p>Hello!</p>;
//   }
// }

// class MenuButton extends Component {
