import React, { Component } from "react";
var xhr;
class IPAddressContainer extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      ip_address: "..."
    };
    this.processRequest = this.processRequest.bind(this);
  }
  componentDidMount() {
    xhr = new XMLHttpRequest();
    xhr.open("GET", "https://ipinfo.io/json?token=2277ce4bac0347", true);
    xhr.send();
    xhr.addEventListener("readystatechange", this.processRequest, false);
  }
  processRequest() {
    if (xhr.readyState === 4 && xhr.status === 200) {
      var response = JSON.parse(xhr.responseText);
      this.setState({
        ip_address: response.ip
      });
    }
  }
  render() {
    return <p>{this.state.ip_address}</p>;
  }
}
export default IPAddressContainer;

// GET /user
// Accept: application/json

// 200 OK
// Content-Type: application/json
// {
//   "name": "...",
//   "url": "http://www...com"
// }
