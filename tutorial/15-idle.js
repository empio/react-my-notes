import React from 'react';

const MINUTES_UNITL_AUTO_LOGOUT = 1;
const CHECK_INTERVAL = 15000;
const STORE_KEY =  'lastAction';
const EVENT_TYPES = [
  'click', 'mouseover', 'mouseout', 'keydown', 'keyup', 'keypress'
];

class Idle extends React.Component {
  constructor(props) {
    super(props);
    this.state = { time: 60 };

    this.check();
    this.initListener();
    // this.initInterval()
  }

  componentDidMount() {
    this.timerID = setInterval(
      () => this.tick(),
      1000
    );
    this.timerIDLE = setInterval(
      () => this.check(),
      CHECK_INTERVAL
    );
  }

  componentWillUnmount() {
    clearInterval(this.timerID, this.timerIDLE);
  }

  tick() {
    this.setState({
      time: this.state.time - 1
    });
  }

  getLastAction() {
    return parseInt(
      sessionStorage.getItem(STORE_KEY)
    );
  }

  check() {
    const now = Date.now();
    const timeleft = this.getLastAction() + MINUTES_UNITL_AUTO_LOGOUT * 60 * 1000;
    const diff = timeleft - now;
    const isTimeout = diff < 0;
    if (isTimeout) {
      alert(this.state.time);
      sessionStorage.clear();
    }
  }

  initListener() {
    EVENT_TYPES.map(
      e => document.body.addEventListener(e, () => this.reset())
    );
  }

  reset() {
    this.setLastAction(Date.now());
    this.setState({
      time: 60
    });
  }

  setLastAction(lastAction) {
    sessionStorage.setItem(
      STORE_KEY, lastAction.toString()
    );
  }

  // initInterval() {
  //   setInterval(
  //     () => this.check(),
  //     CHECK_INTERVAL
  //   );
  // }

  render () {
    return (
      <div
        style={{
          backgroundColor: 'gray',
          height: 100,
          width: 100
        }}
      >
        {this.state.time}
      </div>
    );
  }
}

export default Idle;
